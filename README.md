# Bayesian Inference

Framework for Bayesian Inference. Version under development.  
Currently, the framework allows:
* The use the optimisation algorithm CMA-ES,
* To do some standard Bayesian inference using the Metropolis-Hasting method,
* To do some hierarchical Bayesian inference.  

Some examples (that require the use of Jupyter Notebooks) are presented :
* One example to fit a simple epidemiological model (model SIS) and propagate uncertainties
* One example with hierarchical Bayesian inference

This framework is used in:
* Hermange, G., Vainchenker, W., Plo, I., & Cournède, P. H. (2021). Mathematical modelling, selection and hierarchical inference to determine the minimal dose in IFN alpha therapy against Myeloproliferative Neoplasms. arXiv preprint [arXiv:2112.10688](https://arxiv.org/abs/2112.10688). (see code in: https://gitlab-research.centralesupelec.fr/2012hermangeg/identifiability-base-model)
