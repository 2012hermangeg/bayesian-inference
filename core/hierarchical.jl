"""
    runHierachical(e)
Designed for the particular case where we use a hierarchical framework
"""
function runHierarchical(e)
  if update(e) #In order to compute some intern variables of e and made some check

    typeOfTask = e["task"]
    algo = e["algo"]["type"]

    if typeOfTask == "parameter estimation"
      parameterEstimationHierarchical(e, algo)
    elseif typeOfTask == "Bayes factor"
      bayesFactorHierarchical(e, algo)
    end

  else
    printl("Run aborted")
  end
end

"""
    parameterEstimationHierarchical(e, algo)
estimate the parameter of the model with the desired algo within hierarchical framework
"""
function parameterEstimationHierarchical(e, algo)

  for ind in 1:e["nb ind"]
    e["sub env"][ind]["results"]["sample"]["ExtParameters"] = Array{Float64}(undef, e["nbExtParameters"] , e["algo"]["max iter"])
    e["sub env"][ind]["results"]["sample"]["objective function"] = Array{Float64}(undef, e["algo"]["max iter"])
  end

  e["results"]["sample"]["Hparameters"] = Array{Float64}(undef, length(e["hyperparameters"]), e["algo"]["max iter"])
  e["results"]["sample"]["objective function"] = Array{Float64}(undef,  e["algo"]["max iter"])

  if algo == "Metropolis Hasting"
    
    for ind in 1:e["nb ind"]
      e["sub env"][ind]["results"]["sample"]["acceptation rate"] = Vector{Int}(undef, e["algo"]["max iter"])
    end
    e["results"]["sample"]["acceptation rate"] = Array{Float64}(undef,  e["algo"]["max iter"])

    hierarchicalMetropolisHasting(e, algorithm="classic")

  elseif algo == "Adaptative Metropolis Hasting"
    for ind in 1:e["nb ind"]
      e["sub env"][ind]["results"]["sample"]["acceptation rate"] = Vector{Int}(undef, e["algo"]["max iter"])
      e["sub env"][ind]["results"]["sample"]["corr_var"] = Vector{Float64}(undef,1+round(Int,e["algo"]["max iter"]/e["algo"]["acceptation_range"]))
    end
    e["results"]["sample"]["acceptation rate"] = Array{Float64}(undef,  e["algo"]["max iter"])


    hierarchicalMetropolisHasting(e, algorithm="adaptative")
  else
    println("error: algo not found")
  end
  
end

function bayesFactorHierarchical(e, algo)

  #Indice ou on échantillone posterior
  indexSampling = sample(collect(1:size(e["posterior distribution"],2)), e["algo"]["nb sampling"], replace=true)
  e["results"]["indexSampling"] = indexSampling
  nbParams = e["model"]["nbParameters"]
  computModel = e["model"]["computational model"] #Model
  likelihoodModel = e["likelihood"]["computational likelihood"]


  #Ini results
  for ind in 1:e["nb ind"]
    e["sub env"][ind]["results"]["sample"]["evalPosterior"] = Array{Float64}(undef,  e["algo"]["nb sampling"])
  end
  e["results"]["sample"]["evalPosterior"] = Array{Float64}(undef,  e["algo"]["nb sampling"])
  e["results"]["sample"]["totalLogPosterior"] = Array{Float64}(undef,  e["algo"]["nb sampling"])

  for i in 1:e["algo"]["nb sampling"]
    s = indexSampling[i]
    Hparams = e["posterior distribution"][:,s]
    e["results"]["sample"]["evalPosterior"][i] = prod(pdf.(e["hyperparameters"], Hparams))
    e["results"]["sample"]["totalLogPosterior"][i] = log(e["results"]["sample"]["evalPosterior"][i])

    priorDistr = map(X->X(Hparams),e["ExtParameters"])
    for ind in 1:e["nb ind"]
      X = Dict();
      sample = e["sub env"][ind]["posterior distribution"][:,s]
      X["parameters"] = copy(sample)
      X["t"] = e["sub env"][ind]["data"]["t"]
      X["f"] = Vector{Any}(undef, length( e["sub env"][ind]["data"]["t"]))
      X["input"] = e["sub env"][ind]["input"]
      computModel(X)
      X["data"] = e["sub env"][ind]["data"]["f"]
      X["parameters likelihood"] = sample[nbParams+1:end]
      likelihood = likelihoodModel(X) 

      newPrior = 1.0
      for j in 1:length(priorDistr)
        newPrior *= pdf(priorDistr[j], sample[j])
      end
      posterior = newPrior*likelihood
      e["sub env"][ind]["results"]["sample"]["evalPosterior"][i] = posterior
      e["results"]["sample"]["totalLogPosterior"][i] += log(posterior)
    end
  end

  e["results"]["log Bayes Factor"] = mean(e["results"]["sample"]["totalLogPosterior"])
  e["results"]["Bayes Factor relatif"] = mean(exp.(e["results"]["sample"]["totalLogPosterior"].-e["results"]["log Bayes Factor"] ))


end

"""
    newEnvironmentHierarchical(e)
Initialize some keys only for hierarchical model
"""
function newEnvironmentHierarchical(e, nbInd)
  e["hierarchical"] = true
  e["nb ind"] = nbInd
  e["data"] = Vector{Any}(undef, nbInd)
  e["sub env"] = Array{Any}(undef, nbInd)
  for i in 1:nbInd
    e["data"][i] = Dict()
    e["data"][i]["t"] = []
    e["data"][i]["f"] = []
    e["sub env"][i] = newEnvironment()
  end
  e["hyperparameters"] = []
  e["algo"]["sub algo"] =  Array{Any}(undef, nbInd)
  for i in 1:nbInd
    e["algo"]["sub algo"][i] = Dict()
  end
  e["algo"]["Hparameters"] = Dict()
end

"""
    updateHierarchical(e)
Update internal variables of the meta-env e in case of a hierarchical structure
"""
function updateHierarchical(e)
   
  ok = true
   
  #First, because hierarchical model is based on hyperparameters, the prior distribution have to be function
  for p in 1:length(e["model"]["parameters"])
    if typeof(e["model"]["parameters"][p])<:Distribution
      distr = e["model"]["parameters"][p]
      e["model"]["parameters"][p] = H -> distr  
    end
  end
  for l in 1:length(e["likelihood"]["parameters"])
    if typeof(e["likelihood"]["parameters"][l])<:Distribution
      distr = e["likelihood"]["parameters"][l]
      e["likelihood"]["parameters"][l] = H ->  distr
    end
  end

  for ind in 1:e["nb ind"]
    e["sub env"][ind]["model"] = deepcopy(e["model"])
    e["sub env"][ind]["likelihood"] = deepcopy(e["likelihood"])
    
    e["sub env"][ind]["algo"] = deepcopy(e["algo"]["sub algo"][ind])
    e["sub env"][ind]["algo"]["max iter"] = e["algo"]["max iter"]
    e["sub env"][ind]["algo"]["truncated proposal"] = e["algo"]["truncated proposal"]
    e["sub env"][ind]["algo"]["single reflection"] = copy(e["algo"]["single reflection"])
    
    e["sub env"][ind]["task"] = deepcopy(e["task"])
    e["sub env"][ind]["data"] = deepcopy(e["data"][ind])
    if !update(e["sub env"][ind])
      return false
    end
  end 
   
  if !updateStandard(e)
    return false
  end

  e["results"]["sample"] = Dict()

  return ok;
end
