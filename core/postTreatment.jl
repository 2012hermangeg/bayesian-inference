"""
Some usefull tools to display the results
"""


function getColor(index)
  arr_color=[:lightseagreen, :mediumblue,:black, :purple, :red]
  return arr_color[(index-1)%length(arr_color)+1]
end

function getMarker(index)
  arr_marker = [:circle,  :diamond, :dtriangle, :rect,:utriangle]
  return arr_marker[(index-1)%length(arr_marker)+1]
end


function plot_hist(e; num_var=1, burn=1, results=e["results"]["sample"]["ExtParameters"])
  histogram(results[num_var,burn:end])
end

function plot_chains(e; arr_vars=[1], burn=1, results=e["results"]["sample"]["ExtParameters"])
  plot(results[arr_vars,burn:end]')
end

function compute_autocorrelation(e; j_max=10, burn=1)
  N  = length(e["results"]["sample"]["ExtParameters"][1,burn:end])				    
  mu =  mean(e["results"]["sample"]["ExtParameters"][:,burn:end], dims=2)
  sig2 =  var(e["results"]["sample"]["ExtParameters"][:,burn:end], dims=2)
  e["results"]["autocorrelation"] = Array{Float64}(undef, e["nbExtParameters"], j_max)
  for j in 1:j_max
    for i in 1:e["nbExtParameters"]
      e["results"]["autocorrelation"][i,j] = 1/sig2[i]*1/(N-j)*(e["results"]["sample"]["ExtParameters"][i,burn:end-j].-mu[i])'*(e["results"]["sample"]["ExtParameters"][i,burn+j:end].-mu[i])
    end
  end
end

function plot_ergodic_mean(e; interval=1, arr_vars=[1], burn=1, recompute=false)
  coll = collect(burn:interval:e["algo"]["max iter"])
  if recompute == true || !haskey(e["results"]["sample"], "ergodic mean")
    e["results"]["sample"]["ergodic mean"] = Array{Float64}(undef, e["nbExtParameters"] , length(coll)) 
    for i in 1:length(coll) 
      e["results"]["sample"]["ergodic mean"][:,i] = mean(e["results"]["sample"]["ExtParameters"][:,burn:coll[i]], dims=2)
    end

    plot(coll, e["results"]["sample"]["ergodic mean"][arr_vars,:]')
  else
    plot(coll, e["results"]["sample"]["ergodic mean"][arr_vars,:]')
  end
    
end


function plot_solution(e; 
			plot_data=true, new_plot=true, t_plot=nothing, vect_params=nothing, bounds=nothing,
			plot_title="")
  
  X = Dict();
  X["f"] = Vector{Any}(undef, length(e["data"]["t"]))
  X["t"] = Vector{Any}(undef, length(e["data"]["t"]))
  X["input"] = e["input"]
  if vect_params == nothing
    X["parameters"] = copy(e["results"]["parameters"])
  else 
    X["parameters"]  = vect_params
  end
  if t_plot == nothing
    max_t = maximum(e["data"]["t"][1])
    t_plot = collect(0.0:max_t/1000:max_t)
  end

  for i in 1:length(e["data"]["t"])
    X["t"][i] = copy(t_plot)
  end
  e["model"]["computational model"](X)
  
  if new_plot
    plot()
  else
    plot!()
  end
  
  for i in 1:length(X["f"])
    if bounds == nothing
      plot!(t_plot, X["f"][i], label=string("model ", i), color=getColor(i), linewidth=3)
    else
      plot!(t_plot, X["f"][i],
		    ribbon=(X["f"][i] .- bounds[i][:,1], bounds[i][:,2] .- X["f"][i]),
		    label=string("model ", i), color=getColor(i), linewidth=3)
    end
    if plot_data
      scatter!(e["data"]["t"][i], e["data"]["f"][i], label=string("data ",i), color=getColor(i), marker=getMarker(i))
    end
  end
  plot!(xlabel="time", ylabel="y", title=plot_title)
end

function plot_convergence_logPosterior(e; burn=1)
  nb_posterior = 1
  if e["algo"]["type"] == "CMAES"
    nb_posterior = e["results"]["g"]
  elseif e["algo"]["type"] == "Classical Metropolis Hasting"
    nb_posterior = e["algo"]["max iter"]
  end

  plot(log.(e["results"]["sample"]["objective function"][burn:nb_posterior]), ylabel="log posterior")
end


function plot_acceptation_rate(e; size=1_000)
  e["results"]["sample"]["cumul acceptation"] = copy(e["results"]["sample"]["acceptation rate"])
  cumul_a = e["results"]["sample"]["cumul acceptation"]
  for i in 2:length(cumul_a)
    cumul_a[i] += cumul_a[i-1]
  end

  plot([1/size*(cumul_a[i] - cumul_a[i-size]) for i in (size+1):length(cumul_a)])
end

function plot_identifiability(e)
  plts = []
  for i in 1:e["model"]["nbParameters"]
    push!(plts, plot(e["results"]["identifiability"][i][:,1],
                     log.(e["results"]["identifiability"][i][:,2]),
		     label=i))
  end
  return plts
end
