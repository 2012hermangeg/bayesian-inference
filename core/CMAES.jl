"""
    CMAES(e)
find the mode of the posterior distribution (a maximum)
implementation based on  'CMA Evolution STrategy: A Tutorial' by N. Hansen 2011
"""
function CMAES(e)
  
  ##############################################################
  #####  References to objects of the environment variable #####
  ##############################################################
  #General

  priorDistr = e["ExtParameters"] #Array of prior distributions, both for model and likelihood parameters
  t_data = e["data"]["t"]
  computModel = e["model"]["computational model"] #Model
  likelihoodModel = e["likelihood"]["computational likelihood"]
  data_f = e["data"]["f"] #y data
  nbParams = e["model"]["nbParameters"]

  #Algo specific

  sigmaCMAES = e["algo"]["sigma"] #step-size
  N = e["algo"]["max iter"]
  covMatrix = e["algo"]["variance"]  #covariance matrix

  TolFun = 0.0
  if haskey(e["algo"],"TolFun")
    TolFun = e["algo"]["TolFun"] #1e-12 is a conservative first choice
  end
  logTolFun = 0.0
  if haskey(e["algo"],"logTolFun")
    logTolFun = e["algo"]["logTolFun"] 
  end
  
  conditionCov = haskey(e["algo"], "conditionCov") ? e["algo"]["conditionCov"] : Inf

  ratioLimLambda = 4
  if haskey(e["algo"], "ratio limite lambda")
    ratioLimLambda = e["algo"]["ratio limite lambda"]
  end

  lambdaIni = 0
  if haskey(e["algo"], "lambda")
    lambdaIni = e["algo"]["lambda"]
  end

  nb_min_gen = 1
  if haskey(e["algo"],"min gen tol")
    nb_min_gen = e["algo"]["min gen tol"]
  end
  
  #Prepare saving results

  arraySample = e["results"]["sample"]["ExtParameters"] #Will contain the sample set of the best member of the generation
  array_f = e["results"]["sample"]["objective function"] #will contain the best objective function


  #################
  ##### Algo  #####
  #################

  ##### Set parameters to their default values #####
  n = length(priorDistr) #Parameter space dimension

  #Selection and Recombination
  if lambdaIni == 0
    lambdaIni = 4 + floor(Int, 3*log(n)) #Can also be selected larger - should be for better results
  end
  lambda = lambdaIni
  
  mu = floor(Int, lambda/2)
  w = [log(lambda/2 + 0.5) - log(i) for i in 1:mu]
  S_w = sum(w)
  w = w./S_w
  mu_eff = 1.0/(w'*w)
  #Step-size control
  c_sigma = (mu_eff + 2.0)/(n + mu_eff + 5)
  d_sigma = 1.0 + 2*max(0.0, sqrt((mu_eff - 1.0)/(n + 1.0)) - 1.0) + c_sigma
  #Covariance matrix adaptation
  c_c = (4.0 + mu_eff/n)/(n+4.0+2*mu_eff/n)
  c_1 = 2.0/((n+1.3)^2 + mu_eff)
  alpha_mu = 2.0
  c_mu = min(1-c_1, alpha_mu*(mu_eff - 2 + 1/mu_eff)/((n+2)^2 + alpha_mu*mu_eff/2))
	
  ### Initialization ###
  p_sigma = zeros(n) #evolution path
  p_c = zeros(n) #evolution path
  g = 0
  C = copy(covMatrix)	
  #Problem dependant:
  sigma = sigmaCMAES #stepsize
  m = mean.(priorDistr)  
  if haskey(e["algo"], "start")
    if e["algo"]["start"] == "random"
      m = rand.(priorDistr)
    elseif e["algo"]["start"] == "distribution"
      m = e["algo"]["distribution"]
    end
  end
	
  ### Iterate until termination criterion met ###
  for i in 1:N
    g += 1
    #sample new population of search points
    @label sampling
    zk = rand(MvNormal(zeros(n), Matrix(1.0I, n,n)),lambda)
    factorisation = svd(C) #There might be something more optimised
    B = factorisation.U
    D = sqrt.(factorisation.S).*Matrix(1.0I, n, n)
  	
    #ConditionCov
    if factorisation.S[1]>conditionCov*factorisation.S[end]
      g = g-1
      break
    end

    yk = B*D*zk
    xk = m .+ sigma.*(B*D*zk)
  	
    #Selection and recombination
    fk = Array{Float64}(undef, lambda)
    pk = Array{Float64}(undef, lambda)
    for k in 1:lambda #ToDo: parallelize
      priork = 1.0
      likelihoodk = 1.0
      for j in 1:length(priorDistr)
        priork *= pdf(priorDistr[j], xk[j,k])
      end
      pk[k] = priork
      if priork != 0.0
        #Model evaluation
        X = Dict();
        X["input"] = e["input"]
        X["parameters"] = xk[:,k]
        if e["task"] == "identifiability"
          X["parameters"] = vcat(X["parameters"][1:e["algo"]["num parameter"]-1], 
				  e["algo"]["value parameter"],
				  X["parameters"][e["algo"]["num parameter"]+1:end])
        end
        X["t"] = t_data
        X["f"] = Vector{Any}(undef, length(t_data))
        computModel(X)
        X["data"] = data_f
        X["parameters likelihood"] = xk[nbParams+1:end,k]
        likelihoodk = likelihoodModel(X) 
      end
      fk[k] = priork*likelihoodk #How to be sure that a sufficient number of them will be striclty positive?
    end
	  
    sp = sortperm(fk, rev=true)  
    if pk[sp[mu]] == 0.0 || fk[sp[1]] == 0.0 #Out of domain or very unlikely
      lambda += 1
      if g==1 && fk[sp[1]] != 0.0
        #If still first generation, choose a better initial point
        m = xk[:,sp[1]]
      end
      if lambda/lambdaIni > ratioLimLambda
        g = g-1
	break
      end
      @goto sampling
    end
    array_f[g] = fk[sp[1]]
    arraySample[:,g] = xk[:,sp[1]]

    if haskey(e["algo"],"sav_all_gen")
      for k in 1:e["algo"]["lambda"]
        e["results"]["sample"]["ExtParameters_all"][k][:,g] = xk[:,k]
        e["results"]["sample"]["obj_function_all"][k,g] = fk[k]
      end
    end
   
    y_w = Array(yk[:,sp[1:mu]]*w)
    m = m .+ sigma.*y_w
		
    #Step-size control
    p_sigma = (1-c_sigma).*p_sigma .+ sqrt(c_sigma*(2-c_sigma)*mu_eff)*B*Array(zk[:,sp[1:mu]]*w)
    E_N01 = sqrt(n)*(1-1/(4*n) + 1/(21*n*n)) #Approximation
    sigma = sigma*exp(c_sigma/d_sigma*(sqrt(p_sigma'*p_sigma)/E_N01 - 1))
		
    #Covariance matrix adaptation
    cond_h_sigma = sqrt(p_sigma'*p_sigma/(1-(1-c_sigma)^(2*g+2)))-(1.4+2/(n+1))*E_N01
    h_sigma = cond_h_sigma < 0.0 ? 1.0 : 0.0
    delta_h_sigma = (1-h_sigma)*c_c*(2-c_c)
    p_c = (1-c_c).*p_c + h_sigma*sqrt(c_c*(2-c_c)*mu_eff).*y_w
    C = (1-c_1-c_mu).*C + c_1.*(Array(p_c*p_c') +delta_h_sigma.*C) + c_mu.*Array(yk[:,sp[1:mu]] * Diagonal(w) *(yk[:,sp[1:mu]])')
    
    ### Check stop criterion ###
    #TolFun
    nbLastValues = 10 + floor(Int,1+30*n/lambdaIni)
    if g > nbLastValues && g > nb_min_gen
      maxLastValues = maximum(array_f[g-nbLastValues:g-1])
      deviation = fk[sp[1]]-maxLastValues 
      logDeviation = log(fk[sp[1]])-log(maxLastValues) 
      if deviation > 0.0 && deviation < TolFun
        break
      elseif logDeviation > 0.0 && logDeviation < logTolFun
        break
      end
    end


  end

  ##########################
  ##### Saving Results #####
  ##########################
  e["results"]["C"] = C
  e["results"]["sigma"] = sigma
  e["results"]["m"] = m
  e["results"]["g"] = g
  e["results"]["final lambda"] = lambda
  e["results"]["parameters"] = m
end		
				
