"""
    additiveNormal(X)
Return the likelihood for an additive noise model 
All data series should follow this noise model with a corresponding parameter
Add data within a serie are considered independant
"""
function additiveNormal(X)
  X["likelihood"] = 1.0
  for i in 1:length(X["parameters likelihood"])  
    X["likelihood"] *= pdf(MvNormal(X["f"][i],X["parameters likelihood"][i]),X["data"][i])
  end
  return X["likelihood"]
end

