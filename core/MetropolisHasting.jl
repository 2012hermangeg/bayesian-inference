"""
    classicMetropolisHasting(e)
Create a Markov Chain with a Multivariate Normal proposal
"""
function classicMetropolisHasting(e)

  ### Some parameters - Algo specific
  initialDistribution = nothing #For ExtParameters
  if haskey(e["algo"],"initial distribution")
    initialDistribution = e["algo"]["initial distribution"]
  end
  N = e["algo"]["max iter"]
  varMH = e["algo"]["variance"] #Constant variance for the proposal distribution

  ##### Algo  #####
  initialiseMH(e,initialDistribution=initialDistribution)

  
  ### Iterate until convergence or stop criteria ###
  for i in 2:N
   loopMH(e, i, params=[varMH], proposalIsSym=true)
  end

  e["results"]["parameters"] = e["results"]["sample"]["ExtParameters"][:,end]

end

function AdaptativeMetropolisHasting(e)

  e["verbose"] ? println("# Begin with Adaptative MH") : "" 

  ### Some parameters - Algo specific
  initialDistribution = nothing #For ExtParameters
  if haskey(e["algo"],"initial distribution")
    initialDistribution = e["algo"]["initial distribution"]
  end
  N = e["algo"]["max iter"]
  varMH_ini = e["algo"]["variance"] #Constant variance for the proposal distribution
  
  ##### Algo  #####
  
  initialiseMH(e,initialDistribution=initialDistribution)

  
  ### Iterate until convergence or stop criteria ###
  var_MH = varMH_ini
  for i in 2:N
    if i % e["algo"]["acceptation_range"] == 0
      acc_rate = mean(e["results"]["sample"]["acceptation rate"][i-1-e["algo"]["acceptation_range"]+2:i-1])
      if acc_rate > 0.5
        var_MH *= 1.2
      elseif acc_rate < 0.2
	var_MH *= 0.8
      end
    end
    loopMH(e, i, params=[varMH], proposalIsSym=true)
  end

  e["results"]["parameters"] = e["results"]["sample"]["ExtParameters"][:,end]

end

"""
    loopMH
One iteration in the Metropolis-Hasting algorithm
Sample X(iter) | X(iter-1)
The proposal can be adapted
"""
function loopMH(e, iter; 
		   arraySample = e["results"]["sample"]["ExtParameters"],
                   priorDistr = e["ExtParameters"],
		   proposal=(X;params=params)->MvNormal(X, params[1]),
		   proposalIsSym=false,
		   params = [1.0],
		   hierarchical=false)

  nbParams = e["model"]["nbParameters"]
  computModel = e["model"]["computational model"] #Model
  likelihoodModel = e["likelihood"]["computational likelihood"]
  array_posterior = e["results"]["sample"]["objective function"]

  #New sample
  newSample = rand(proposal(arraySample[:,iter-1], params=params))

  #Posterior computation
  newPrior = 1.0
  logNewPrior = 0.0
  newLikelihood = 1.0

  for j in 1:length(priorDistr)
    if e["algo"]["single reflection"]["ind"] == j
      proposalIsSym = false
      if e["algo"]["single reflection"]["bound"] == "lower"
        if newSample[j] < minimum(priorDistr[j])
          newSample[j] = 2*minimum(priorDistr[j]) - newSample[j] 
        end
      elseif e["algo"]["single reflection"]["bound"] == "upper"
        if newSample[j] > maximum(priorDistr[j])
          newSample[j] = 2*maximum(priorDistr[j]) - newSample[j] 
        end
      end
   end


#        if newSample[j] < minimum(priorDistr[j])
#          println("too low")
#          newSample[j] = 2*minimum(priorDistr[j]) - newSample[j] 
#        end
#        if newSample[j] > maximum(priorDistr[j])
#          println("too high")
#          newSample[j] = 2*maximum(priorDistr[j]) - newSample[j] 
#        end
    
    
    newPrior *= pdf(priorDistr[j], newSample[j])
    #if newPrior == 0
    #  @show j
    #  @show priorDistr[j]
    #end
    logNewPrior += log(pdf(priorDistr[j], newSample[j]))
  end


  if newPrior != 0.0
    #Model evaluation
    X = Dict();
    X["input"] = e["input"]
    X["parameters"] = copy(newSample)
    X["t"] = e["data"]["t"]
    X["f"] = Vector{Any}(undef, length(e["data"]["t"]))
    computModel(X)

    X["data"] = e["data"]["f"]
    X["parameters likelihood"] = newSample[nbParams+1:end]
    newLikelihood = likelihoodModel(X) 
  elseif  logNewPrior != -Inf
  else
   # println("prior 0")
  end
    
  #Verification if accepted or not
  ratio_proposal = 1.0
  if !proposalIsSym
    numerator = pdf(proposal(newSample,params=params),arraySample[:,iter-1])
    denominator = pdf(proposal(arraySample[:,iter-1],params=params),newSample)
    if e["algo"]["single reflection"]["ind"] != 0 #0 is default case
      ind = e["algo"]["single reflection"]["ind"]
      borne = e["algo"]["single reflection"]["bound"] == "lower" ? minimum(priorDistr[ind]) :  maximum(priorDistr[ind])
      sym_iMinusOne = copy(arraySample[:,iter-1])
      sym_iMinusOne[ind] = 2*borne - arraySample[ind,iter-1]
      sym_i = copy(newSample)
      sym_i[ind] = 2*borne - newSample[ind]
      numerator += pdf(proposal(newSample,params=params),sym_iMinusOne)
      denominator += pdf(proposal(arraySample[:,iter-1],params=params),sym_i)
    end
    ratio_proposal = numerator/denominator
               
  end
  
  denom = array_posterior[iter-1]
  oldPrior = 1.0
  if hierarchical
    for j in 1:length(priorDistr)
      oldPrior *= pdf(priorDistr[j], arraySample[j, iter-1])
    end
    denom *= oldPrior
  end

  if newPrior*newLikelihood/denom*ratio_proposal >= rand()
    #Accepted
    arraySample[:, iter] = copy(newSample)
    e["results"]["sample"]["acceptation rate"][iter] = 1      
    array_posterior[iter] = hierarchical ? newLikelihood : newPrior*newLikelihood
  else
    #Rejected
    arraySample[:, iter] = copy(arraySample[:, iter-1])
    e["results"]["sample"]["acceptation rate"][iter] = 0 
    array_posterior[iter] = array_posterior[iter-1]
  end
  
end

function initialiseMH(e;
		       arraySample = e["results"]["sample"]["ExtParameters"],
                       priorDistr = e["ExtParameters"],
                       initialDistribution=nothing,
		       hierarchical=false)
  
  nbParams = e["model"]["nbParameters"]
  computModel = e["model"]["computational model"] #Model
  likelihoodModel = e["likelihood"]["computational likelihood"]
  array_posterior = e["results"]["sample"]["objective function"]
  
  ### Initialisation ###

  #Initiale sample array
  oldSample = rand.(priorDistr)
  if initialDistribution != nothing
    oldSample = initialDistribution
  end
  arraySample[:, 1] = copy(oldSample)

  #Evaluate model
  X = Dict();
  X["parameters"] = copy(oldSample)
  X["input"] = e["input"]
  X["t"] = e["data"]["t"]
  X["f"] = Vector{Any}(undef, length(e["data"]["t"]))
  computModel(X)

  #compute posterior
  oldPrior = 1.0
  oldLikelihood = 1.0
  for j in 1:length(priorDistr)
    oldPrior *= pdf(priorDistr[j], oldSample[j])
  end
 
  X["data"] = e["data"]["f"]
  X["parameters likelihood"] = oldSample[nbParams+1:end]
  oldLikelihood = likelihoodModel(X) 
  if hierarchical
    array_posterior[1] = oldLikelihood
  else
    array_posterior[1] = oldPrior*oldLikelihood
  end
  e["results"]["sample"]["acceptation rate"][1] = 1

end

function computePosteriorHyperParameters(e, iter_HP; iter_param=iter_HP)
  X = e["results"]["sample"]["Hparameters"][:,iter_HP]
  prior_HP = 1.0
  for i in 1:length(X)
    prior_HP *= pdf(e["hyperparameters"][i], X[i])
  end
  prior_theta_knowing_HP = 1.0
  if prior_HP != 0.0
    priorDistr = map(z->z(X),e["ExtParameters"])
    for ind in 1:e["nb ind"]
      for i in 1:length(priorDistr)
        prior_theta_knowing_HP *= pdf(priorDistr[i],e["sub env"][ind]["results"]["sample"]["ExtParameters"][i,iter_param])
      end
    end
  end
  return prior_HP*prior_theta_knowing_HP
end

function computeHyperParameters(e, i)

  if e["algo"]["Hparameters"]["type"] == "Classical Metropolis Hasting"
    e["results"]["sample"]["Hparameters"][:,i] = rand(MvNormal(e["results"]["sample"]["Hparameters"][:,i-1],e["algo"]["Hparameters"]["variance"]))
    denom = computePosteriorHyperParameters(e,i-1)
    e["results"]["sample"]["objective function"][i-1] = denom 

    #NB: The proposal distribution is symmetric
    if computePosteriorHyperParameters(e,i, iter_param=i-1)/denom>=rand()
      e["results"]["sample"]["acceptation rate"][i] = 1
    else
      e["results"]["sample"]["Hparameters"][:,i] =  e["results"]["sample"]["Hparameters"][:,i-1] 
      e["results"]["sample"]["acceptation rate"][i] = 0
    end
  elseif e["algo"]["Hparameters"]["type"] == "Gibbs"
    e["algo"]["Hparameters"]["proposal"](e, i)
    e["results"]["sample"]["acceptation rate"][i] = 1
  end

end
"""
    hierarchicalClassicMetropolisHasting(e)
Classic MH algorithm used within a hierarchical framework
Symmetric Multivariate Normal proposal for both individuals' and hyper-parameters
"""
function hierarchicalMetropolisHasting(e; algorithm="classic")

  #Initialization
  e["verbose"] ? println("Initialize hierarchical MH with algo=", algorithm) : ""

  #First: Hyperparameters of the model
  Hparams = zeros(length(e["hyperparameters"]))
  if haskey(e["algo"]["Hparameters"],"initial distribution")
    Hparams = e["algo"]["Hparameters"]["initial distribution"]
  else
    Hparams = rand.(e["hyperparameters"])
  end
  e["results"]["sample"]["Hparameters"][:,1] = copy(Hparams)
  e["results"]["sample"]["acceptation rate"][1] = 1

  update_corr_sig = [true for ind in 1:e["nb ind"]]

  for ind in 1:e["nb ind"]
    initialDistribution = nothing #For ExtParameters
    if haskey(e["sub env"][ind]["algo"],"initial distribution")
      initialDistribution = e["sub env"][ind]["algo"]["initial distribution"]
    end
    if algorithm == "adaptative"
      e["sub env"][ind]["results"]["sample"]["corr_var"][1] =  e["sub env"][ind]["algo"]["corr_var"] 
    end

    initialiseMH(e["sub env"][ind],initialDistribution=initialDistribution,
		  priorDistr=map(X->X(Hparams),e["sub env"][ind]["ExtParameters"]), hierarchical=true) 
  end

  e["verbose"] ? println("run loop MH") : ""
  
    ### Iterate until convergence or stop criteria ###
  for i in 2:e["algo"]["max iter"]
    
    #Begin with hyperparameters
    computeHyperParameters(e, i)

    #Then parameters for each individual
    Hparams = copy(e["results"]["sample"]["Hparameters"][:,i])
    priorDistr = map(X->X(Hparams),e["ExtParameters"])
      


    for ind in 1:e["nb ind"]
      if algorithm == "adaptative"
        if  i % e["algo"]["acceptation_range"] == 0
          acc_rate = mean(e["sub env"][ind]["results"]["sample"]["acceptation rate"][i-1-e["algo"]["acceptation_range"]+2:i-1])

	  if (acc_rate > 0.9 || acc_rate < 0.05) && update_corr_sig[ind] == false 
	    update_corr_sig[ind] = true
	    e["verbose"] ? println("acc rate exits limits for ind=", ind) : ""
	  end

	  if update_corr_sig[ind]
            if acc_rate > 0.99
	      e["sub env"][ind]["algo"]["corr_var"] *= 10
            elseif acc_rate > 0.9
	      e["sub env"][ind]["algo"]["corr_var"] *= 3
            elseif acc_rate > 0.7
	      e["sub env"][ind]["algo"]["corr_var"] *= 2
            elseif acc_rate > 0.5
	      e["sub env"][ind]["algo"]["corr_var"] *= 1.5
            elseif acc_rate < 0.2
	      e["sub env"][ind]["algo"]["corr_var"] *= 0.9
            elseif acc_rate < 0.15
	      e["sub env"][ind]["algo"]["corr_var"] *= 0.7
            elseif acc_rate < 0.1
	      e["sub env"][ind]["algo"]["corr_var"] *= 0.5
            elseif acc_rate < 0.05
	      e["sub env"][ind]["algo"]["corr_var"] *= 0.3
            elseif acc_rate < 0.01
	      e["sub env"][ind]["algo"]["corr_var"] *= 0.1
            elseif acc_rate < 0.005
	      e["sub env"][ind]["algo"]["corr_var"] *= 0.01
            elseif acc_rate < 0.001
	      e["sub env"][ind]["algo"]["corr_var"] *= 0.001
            end
	  end
         ind_loc = 1+round(Int,i/e["algo"]["acceptation_range"])
	 e["sub env"][ind]["results"]["sample"]["corr_var"][ind_loc] =  e["sub env"][ind]["algo"]["corr_var"]

	   if length(unique(e["sub env"][ind]["results"]["sample"]["corr_var"][max(1,ind_loc-20):ind_loc])) == 1
	     update_corr_sig[ind] = false
	   end

        end
      end

      loopMH(e["sub env"][ind], i, params=e["sub env"][ind]["algo"]["corr_var"].*[e["sub env"][ind]["algo"]["variance"]],
				priorDistr=priorDistr, proposalIsSym=true, hierarchical=true)
    end
  end 

  #Saving results
  for ind in 1:e["nb ind"]
    e["sub env"][ind]["results"]["parameters"] = e["sub env"][ind]["results"]["sample"]["ExtParameters"][:,end]
  end
  e["results"]["parameters"] = e["results"]["sample"]["Hparameters"][:,end]

  e["results"]["sample"]["objective function"][e["algo"]["max iter"]] = computePosteriorHyperParameters(e,e["algo"]["max iter"]) 
end
