"""
Core of the framework
Assign the job to the different tasks or algorithms
"""


"""
run(e)
run the task with the chosen algo
"""
function run(e)
  
  if e["hierarchical"] == false
    if update(e) #In order to compute some intern variables of e and made some check

      typeOfTask = e["task"]
      algo = e["algo"]["type"]
    
      if typeOfTask == "parameter estimation"
	parameterEstimation(e, algo)
      elseif typeOfTask == "identifiability"
	identifiability(e, algo)
      elseif typeOfTask == "uncertainty propagation"
	uncertaintyPropagation(e, algo) 
      else
	
      end
    
    else
      prinltn("Run aborted");
    end
  else 
    runHierarchical(e)									
  end


end

"""
parameterEstimation(e, algo)
estimate the parameters of the model using the chosen algo
"""
function parameterEstimation(e, algo)
  
  e["results"]["sample"]["ExtParameters"] = Array{Float64}(undef, e["nbExtParameters"] , e["algo"]["max iter"])
  e["results"]["sample"]["objective function"] = Array{Float64}(undef, e["algo"]["max iter"])
 
  if algo == "Classical Metropolis Hasting"
    #Check also that all is defined correctly

    e["results"]["sample"]["acceptation rate"] = Vector{Int}(undef, e["algo"]["max iter"])
    classicMetropolisHasting(e)

  elseif algo == "Adadptative Metropolis Hasting"
    e["results"]["sample"]["acceptation rate"] = Vector{Int}(undef, e["algo"]["max iter"])
    AdaptativeMetropolisHasting(e)

  elseif algo == "CMAES"
    if haskey(e["algo"],"sav_all_gen")
      e["results"]["sample"]["ExtParameters_all"] = Array{Any}(undef, e["algo"]["lambda"])
      for i in 1:e["algo"]["lambda"]
        e["results"]["sample"]["ExtParameters_all"][i] = Array{Float64}(undef, e["nbExtParameters"] , e["algo"]["max iter"])
      end
      e["results"]["sample"]["obj_function_all"] = Array{Float64}(undef, e["algo"]["lambda"] , e["algo"]["max iter"])
    end
    CMAES(e)
  else
    println("error: algorithm not found")
  end

end

function identifiability(e, algo)
  
  e["results"]["identifiability"] = Array{Any}(undef, e["model"]["nbParameters"])
  
  for i in 1:e["model"]["nbParameters"]
    e["results"]["identifiability"][i] = Array{Float64}(undef, length(e["model"]["parameters range"][i]), 3)
    e["results"]["identifiability"][i][:,1] = e["model"]["parameters range"][i]
    
    for t in 1:length(e["model"]["parameters range"][i])
      theta_i = e["model"]["parameters range"][i][t]
      e_bis = deepcopy(e)
      e_bis["model"]["parameters"] = e_bis["model"]["parameters"][1:end .!= i] 
      e_bis["algo"]["variance"] = e_bis["algo"]["variance"][1:end .!= i, 1:end .!= i]
      e_bis["algo"]["num parameter"] = i
      e_bis["algo"]["value parameter"] = theta_i
      update(e_bis)
      if algo == "CMAES"
        parameterEstimation(e_bis, algo)
        e["results"]["identifiability"][i][t,2] = e_bis["results"]["g"] == 0 ? 0.0 : e_bis["results"]["sample"]["objective function"][e_bis["results"]["g"]]
        e["results"]["identifiability"][i][t,3] = e_bis["results"]["g"]
      else 
        println("no algo defined for identifiability!")
        break
      end
      
    end
  end

end

function uncertaintyPropagation(e, algo)
  e["results"]["sample"]["y"] = Array{Any}(undef, length(e["algo"]["t"]))

  if algo == "Monte-Carlo"
    t_data = deepcopy(e["algo"]["t"])
    for i in 1:length(e["algo"]["t"])
      e["results"]["sample"]["y"][i] = Array{Float64}(undef, length(e["algo"]["t"][i]), e["algo"]["nb sampling"]) 
    end

    indexSampling = sample(collect(1:size(e["posterior distribution"],2)), e["algo"]["nb sampling"], replace=true)
    e["results"]["parameters"] = mean(e["posterior distribution"][:,indexSampling], dims=2)

    for i in 1:e["algo"]["nb sampling"]
      s = indexSampling[i]

      #Model evaluation
      X = Dict();
      X["parameters"] = e["posterior distribution"][:,s]
      X["t"] = t_data
      X["f"] = Vector{Any}(undef, length(t_data))
      X["input"] = e["input"]
      e["model"]["computational model"](X)

      for j in 1:length(e["algo"]["t"])
	e["results"]["sample"]["y"][j][:,i] = X["f"][j]
      end
    end
    arrayQuantiles = e["algo"]["arrayQuantiles"]
    e["results"]["y_perc"] =  Array{Any}(undef, length(e["algo"]["t"])) 


    for i in 1:length(e["algo"]["t"])
      e["results"]["y_perc"][i] =  Array{Float64}(undef, length(e["algo"]["t"][i]), length(arrayQuantiles))
      for t in 1:length(e["algo"]["t"][i])
	e["results"]["y_perc"][i][t,:] = quantile(e["results"]["sample"]["y"][i][t,:], arrayQuantiles)
      end
    end
  end
end



