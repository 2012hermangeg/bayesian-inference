"""
Definition of the environment e
This is a dictionnary used in all the framework
Contains all that is necessary for the computation of the algorithms
"""

using Distributions, Random, LinearAlgebra
using HDF5, JLD2, Dates, FileIO
ENV["GKSwstype"]="nul"
using Plots;
include("run.jl")
include("hierarchical.jl")
include("postTreatment.jl")
include("likelihood.jl")

### All algorithms included ###
include("MetropolisHasting.jl")
include("CMAES.jl")

"""
    newEnvironement()
return a new environment (dict) initialized by default
"""
function newEnvironment(; seed=nothing, hierarchical=false, nbInd=1, verbose=false)
  e = Dict()
  e["date"] = now()
  e["seed"] = seed
  e["restart"] = false
  e["verbose"] = verbose

  #Model parameters
  e["model"] = Dict()
  e["model"]["parameters"] = []
  e["model"]["parameters range"] = []

  #Likelihood model
  e["likelihood"] = Dict()
  e["likelihood"]["parameters"] = []

  #Setting for the algorithm
  e["algo"] = Dict()
  e["algo"]["max iter"] = 999_999 #default number of iteration for the algorithms
  e["algo"]["truncated proposal"] = false #Especially for MH
  e["algo"]["single reflection"] = Dict()
  e["algo"]["single reflection"]["ind"] = 0
  e["algo"]["single reflection"]["bound"] = "" #lower or upper
  e["hierarchical"] = false

  #Data
  e["data"] = Dict()
  e["data"]["t"] = []
  e["data"]["f"] = []

  e["input"] = Dict()
  if hierarchical
    newEnvironmentHierarchical(e, nbInd)
  end


  e["verbose"] ? println("environment initialized") : "" 

  return e;
end

##############################################
### Functions related to the environment e ###
##############################################



"""
    updateStandard(e)
update internal variables in (sub)environment e
"""
function updateStandard(e)

  ok = true

  #Model
  e["model"]["nbParameters"] = length(e["model"]["parameters"]);

  #Likelihood
  e["likelihood"]["nbParameters"] = length(e["likelihood"]["parameters"]);

  #Likelihood
  if e["likelihood"]["type"] != "perso"
    if e["likelihood"]["type"] == "additive normal"
      e["likelihood"]["computational likelihood"] = additiveNormal
    else
      #Error
      println("No corresponding likelihood model defined")
      ok = false
    end
  end

  #Aggregate model and likelihood parameters
  e["ExtParameters"] = vcat(e["model"]["parameters"], e["likelihood"]["parameters"])
  e["nbExtParameters"] = e["model"]["nbParameters"] + e["likelihood"]["nbParameters"]


  #Dictionnary for the results of the different tasks
  if e["restart"] == "restart with historic"
    if !haskey(e, "hist results")
      e["hist results"] = Array()
    end
    push!(e["hist results"], deepcopy(e["results"]))
  end
  e["results"] = Dict()
  e["results"]["sample"] = Dict()


  return ok;
end

"""
    update(e)
update internal variables in environment e
"""
function update(e)

  ok = true


  if e["hierarchical"]
    ok = updateHierarchical(e)
  else
    ok = updateStandard(e)
  end

  return ok
end
